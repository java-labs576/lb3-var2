package com.example.Lb3_var2;

import java.io.IOException;

import java.util.Map;
import java.util.HashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.servlet.annotation.*;

@WebServlet(name = "handlerServlet", value = "/handler")
public class HandlerServlet extends HttpServlet {
    private int count;
    private long sum;
    private Map<Integer, Integer> map = new HashMap<>();

    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
                                                throws IOException {
        String str_age = request.getParameter("age");
        int age = Integer.parseInt(str_age);
        if (map.containsKey(age)) {
            int saved = map.get(age);
            ++saved;
            map.replace(age, saved);
        } else {
            map.put(age, 1);
        }
        ++count;
        sum += age;

        HttpSession session = request.getSession();

        session.setAttribute("count", count);
        int k = map.get(age);
        session.setAttribute("number", k);
        session.setAttribute("age", age);
        session.setAttribute("percent", 100 * k / count);
        session.setAttribute("average", sum / count);

        response.sendRedirect("./answer.jsp");
    }

    public void destroy() {
    }
}