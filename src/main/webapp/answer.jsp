<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
  <meta charset="UTF-8">
  <title>Результат</title>
</head>
<body>

<h1>Результат</h1>
<p>
  Всего проголосовало человек: ${count} из них ${number} имеют возраст ${age}.
  Єто ${percent}%.
</p>
<p>
  Средний возраст всех проголосовавших - ${average}.
</p>
<a href=".">&lt Назад</a>

</body>
</html>
